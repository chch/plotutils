#!/bin/bash

# =========================================================================== #
# WHAT YOU SEE IS WHAT YOU TRACE                                              #
# =========================================================================== #
# Copyright (C) 2023 Christoph Haag                                           #
# --------------------------------------------------------------------------- #
# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details.
# -> http://www.gnu.org/licenses/gpl.txt

# =========================================================================== #
# C O N F I G U R E
# =========================================================================== #
  TMP="/tmp/WYSWYT${RANDOM}"
# --------------------------------------------------------------------------- #
  INKSCAPEEXTENSION="lib/python"
  export PYTHONPATH="$INKSCAPEEXTENSION"
  APPLYTRANSFORMPY="lib/python/applytransform.py"
  APPLYTRANSFORMPY=`realpath $APPLYTRANSFORMPY`
# --------------------------------------------------------------------------- #
# U S E R   I N T E R A C T I O N
# --------------------------------------------------------------------------- #
  SVG=`echo $* | sed 's/ /\n/g' | #
       grep "\.svg$" | grep -v WYSIWYD | head -n 1` 
# --------------------------------------------------------------------------- #
  if [ ! -f $SVG ] ||
     [ `echo "$SVG" | wc -c` -lt 2 ]
   then echo; echo "A SVG FILE PLEASE!"
              echo "e.g. $0 20161110212041.svg"; echo
        exit 0;
  fi
# --------------------------------------------------------------------------- #
  OUTDIR="_";SVGOUT="$OUTDIR/"`basename $SVG | sed 's/\.svg$/_WYSIWYD.svg/'`
# --------------------------------------------------------------------------- #
  if [ -f $SVGOUT ]; then
       echo "$SVGOUT DOES EXIST"
       read -p "OVERWRITE $SVGOUT? [y/n] " ANSWER
       if [ X$ANSWER != Xy ] ; then echo "Bye"; exit 1; else echo; fi
  fi
# --------------------------------------------------------------------------- #
# SET TRACEWIDTH
# --------------------------------------------------------------------------- #
  TRACEWIDTH=`echo $* | sed 's/ /\n/g'    | #
              grep "^--tracewidth"        | #
              head -n 1 | cut -d "=" -f 2 | #
              grep '^[0-9]\{1,\}$'`         #
  if [ "$TRACEWIDTH" == "" ];then TRACEWIDTH="5000";fi
# --------------------------------------------------------------------------- #
# SET FLOODFILL
# --------------------------------------------------------------------------- #
  FLOODFILL=`echo $* | sed 's/ /\n/g'    | #
             grep "^--flood" | head -n 1 | #
             cut -d "=" -f 2             | #
             grep '^[0-9a-fA-F]\{6\}$'`    #
# --------------------------------------------------------------------------- #
  HOW=`echo $* | sed 's/ /\n/g' |      # GET HOW FILE FROM CLI ...
       grep "\.how$" | head -n 1`      # ... ARGUMENTS (FIRST ONE ONLY)
  cat "$HOW" > ${TMP}.how 2> /dev/null # CREATE FILE (EVEN EMPTY)
  HOW=${TMP}.how                       # USE TMP HOW FILE NOW
# --------------------------------------------------------------------------- #
# SET DEDICATED COLORS
# --------------------------------------------------------------------------- #
  NOCOLOR="#ffffff"
  DONTTRACE=`echo $* | sed 's/ /\n/g'    | #
             grep "^--donttrace"         | # 
             head -n 1 | cut -d "=" -f 2 | #
             grep '[0-9a-fA-F|]'         | #
             sed 's/[^a-fA-F0-9|]//g'`     #
  if [ "$DONTTRACE" == "" ]
  then  DONTTRACE=`sed -e '/./{H;$!d;}'                           \
                       -e 'x;/NOT TRACE/!d;/#[0-9a-fA-F]\{6\}/!d' \
                       -e 's/ /\n/g' $HOW     | #
                   grep "^#[0-9a-f]\{6\}"     | #
                   sed ':a;N;$!ba;s/\n/|/g'   | #
                   sed 's/[^a-fA-F0-9|]//g'`    #
  fi
  if [ `echo $DONTTRACE | wc -c` -lt 6 ];then DONTTRACE="XXXXXXX";fi
# ----
  OVERDRAW=`echo $* | sed 's/ /\n/g'    | #
            grep "^--overdraw"          | # 
            head -n 1 | cut -d "=" -f 2 | #
            grep '[0-9a-fA-F|]'         | #
            sed 's/[^a-fA-F0-9|]//g'`     #
  if [ "$OVERDRAW" == "" ]
  then  OVERDRAW=`sed -e '/./{H;$!d;}'                           \
                      -e 'x;/OVERDRAW/!d;/#[0-9a-fA-F]\{6\}/!d' \
                      -e 's/ /\n/g' $HOW   | #
                  grep "^#[0-9a-f]\{6\}"   | #
                  sed ':a;N;$!ba;s/\n/|/g' | #
                  sed 's/[^a-fA-F0-9|]//g'`  #
  fi
  if [ `echo $OVERDRAW | wc -c` -lt 6 ];then OVERDRAW="XXXXXXX";fi
# --------------------------------------------------------------------------- #
  RNDMZE=`echo $* | sed 's/ /\n/g' | #
          grep '^--randomize'`
  if [ "$RNDMZE" != "" ]
  then RNDMSEED=`echo $RNDMZE           | #
                 grep -- '--randomize=' | #
                 cut -d '=' -f 2`         #
       if [ "$RNDMSEED" == "" ];then RNDMSEED="$SVGOUT";fi
       RNDMZE="shuf --random-source=<(mkseed $RNDMSEED)"
  fi
# --------------------------------------------------------------------------- #
 #source lib/sh/headless.functions
  source lib/sh/trace.functions
  source lib/sh/shuffle.functions
# --------------------------------------------------------------------------- #
# F L A T T E N   S V G
# --------------------------------------------------------------------------- #
  inkscape --export-type=pdf           \
           --export-filename=${TMP}.pdf \
             $SVG 2> /dev/null
  inkscape --export-plain-svg                \
           --export-filename=${TMP}.PLAIN.svg \
             ${TMP}.pdf 2> /dev/null
  SVG=${TMP}.FLAT.svg
  python3 $APPLYTRANSFORMPY ${TMP}.PLAIN.svg | sed 's/<\/*g[^>]*>//g' > $SVG
# --------------------------------------------------------------------------- #
# S E T   V A L U E S 
# --------------------------------------------------------------------------- #
  SVGW=`cat $SVG | sed 's/width=/\n&/g'   | #
        grep "^width" | head -n 1         | #
        cut -d "\"" -f 2 | cut -d "." -f 1` #
  SVGH=`cat $SVG | sed 's/height=/\n&/g'  | #
        grep "^height" | head -n 1        | #
        cut -d "\"" -f 2 | cut -d "." -f 1` #
  SF=`python -c "print ${SVGW}.00000 / ${TRACEWIDTH}"`
  TS=`python -c "print 2.0 * $SF"`;PS=`python -c "print $TS * 3.0"`
  FILLCOLORS=`sed 's/fill:#[a-fA-F0-9]\{6\}/\n&\n/g' $SVG | #
              egrep "^fill" | sort -u`
  STROKECOLORS=`sed 's/stroke:#[a-fA-F0-9]\{6\}/\n&\n/g' $SVG | #
                egrep "^stroke" | sort -u`
# --------------------------------------------------------------------------- #
# C R E A T E   E M P T Y   S V G
# --------------------------------------------------------------------------- #
  echo "<?xml version=\"1.0\" 
              encoding=\"UTF-8\" 
              standalone=\"no\"?>
        <svg width=\"$SVGW\" height=\"$SVGH\" 
             id=\"svg\" version=\"1.1\"
             xmlns=\"http://www.w3.org/2000/svg\"
             xmlns:sodipodi=
           \"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"
             xmlns:inkscape=
           \"http://www.inkscape.org/namespaces/inkscape\">
        <defs id=\"defs\"/>" | \
  sed ':a;N;$!ba;s/\n//g'    |  tr -s ' '                          >  $SVGOUT
  echo "</svg>"                                                    >> $SVGOUT
# =========================================================================== #
# T R A C E   F I L L S
# =========================================================================== #
  for FILL in `echo $FILLCOLORS | #
               sed 's/ /\n/g'   | #
               grep -v $NOCOLOR`  # 
   do echo "PROCESSING: $FILL"
      COLORNAME=`echo $FILL   | #
                 cut -d ":" -f 2 | #
                 sed 's/[^a-fA-F0-9]//g'`
      COLORSVG="${TMP}fill:${COLORNAME}.svg"
    # -------------------------------------------------------------------- #
    # KEEP CURRENT COLOR ONLY (THE REST: REMOVE OR MAKE COVER)
    # -------------------------------------------------------------------- #
      if [ `echo "$FILL" | egrep "$DONTTRACE" | wc -l` -ge 1 ]
      then DISABLECOLOR="none"; else  DISABLECOLOR="$NOCOLOR"; fi

      cp $SVG $COLORSVG
      sed -i "s/$FILL/XXXXXX/g"                          $COLORSVG
      sed -i "s/:#[a-fA-F0-9]\{6\}/:$DISABLECOLOR/g"     $COLORSVG
      sed -i "s/stroke:#[a-fA-F0-9]\{6\}/stroke:none/g"  $COLORSVG
      sed -i "s/XXXXXX/fill:#000000/g"                   $COLORSVG

      FILLTRACEADD --source=$COLORSVG \
                   --target=$SVGOUT    \
                   --targetfill=$COLORNAME

  done
# =========================================================================== #
  if [ "$FLOODFILL" != "" ]
  then  FLOODFILL --source=$SVG --target=$SVGOUT --targetfill=$FLOODFILL  ;fi
# =========================================================================== #
# T R A C E   S T R O K E S
# =========================================================================== #
  for STROKE in `echo $STROKECOLORS            | #
                 sed 's/ /\n/g'                | #
                 egrep -v "$NOCOLOR|$DONTTRACE"` # 
   do echo "PROCESSING: $STROKE"
      COLORNAME=`echo $STROKE | #
                 cut -d ":" -f 2 | #
                 sed 's/[^a-fA-F0-9]//g'`
      COLORSVG="${TMP}stroke:${COLORNAME}.svg"

      if [ "$OVERDRAW" != "XXXXXX" ]
      then if [ `echo $STROKE   | egrep "$OVERDRAW" | wc -l` -ge 1 ]
           then  sed "s/$STROKE/::XXXXXX::/g" $SVG  | #
                 sed "s/:$NOCOLOR/::NNNNNN::/g"     | #
                 sed "s/:#[a-fA-F0-9]\{6\}/:none/g" | #
                 sed "s/::NNNNNN::/:$NOCOLOR/g"     | # 
                 sed "s/::XXXXXX::/$STROKE/g"       > $COLORSVG
           else  cp $SVG $COLORSVG
                 for ODCOLOR in `echo $OVERDRAW | sed 's/|/\n/g'`
                  do sed -i "s/:#$ODCOLOR/:none/g" $COLORSVG
                 done
           fi
      else       cp $SVG $COLORSVG
      fi

      sed -i "s/$STROKE/XXXXXX/g"                                  $COLORSVG
      sed -i "s/:#[a-fA-F0-9]\{6\}/:$NOCOLOR/g"                    $COLORSVG
      sed -i "s/XXXXXX/$STROKE/g"                                  $COLORSVG
      sed -i ':a;N;$!ba;s/\n/ /g'                                  $COLORSVG
      sed -i "s/<path[^>]*>/\n&&\n/g"                              $COLORSVG
      sed -i "/^<path/s/stroke-width:[0-9.]*/stroke-width:$PS/"    $COLORSVG
      sed -i "/^<path/s/stroke-width:[0-9.]*/stroke-width:$TS/2"   $COLORSVG
      sed -i "/^<path/s/stroke:#[a-fA-F0-9]\{6\}/stroke:$NOCOLOR/" $COLORSVG
      sed -i "s/stroke-linejoin:[a-z]*/stroke-linejoin:bevel/"     $COLORSVG
      sed -i "s/stroke-linecap:[a-z]*/stroke-linecap:butt/"        $COLORSVG

      STROKETRACEADD --source=$COLORSVG \
                     --target=$SVGOUT    \
                     --targetstroke=$COLORNAME

  done
# =========================================================================== #
# A P P E N D   S T R O K E S  (WITHOUT TRACING)
# =========================================================================== #
  LNAME=`echo $DONTTRACE | sed 's/#//g' | sed 's/ //'`
  DONTRACETHESESTROKE=`echo $DONTTRACE | sed 's/#/stroke:#/g'`

  sed ':a;N;$!ba;s/\n/ /g' $SVG                  | #
  sed 's/<path/\n&/g' | sed '/^<path/s/>/&\n/'   | #
  sed 's/fill:#[0-9a-fA-F]\{6\}/fill:none/g'     | #
  egrep -i "$DONTRACETHESESTROKE"  >> ${TMP}UNTRACED.svg
 
  if [ `wc -l ${TMP}UNTRACED.svg | cut -d ' ' -f 1` -gt 0 ]
  then
        sed -i '/^<\/svg>$/d'                            $SVGOUT
        echo "<g inkscape:groupmode=\"layer\" \
                 inkscape:label=\"untraced:$LNAME\" \
              >" | tr -s ' '                          >> $SVGOUT
        sed "s/ id=\"[^\"]*\"/ /g" ${TMP}UNTRACED.svg >> $SVGOUT
        echo "</g>"                                   >> $SVGOUT
        echo "</svg>"                                 >> $SVGOUT
  fi
# --------------------------------------------------------------------------- #
# F I N A L I Z E   S V G
# --------------------------------------------------------------------------- #
  B=::`echo ${RANDOM} | md5sum | cut -c 1-4`::
  if [ `grep 'transform=' $SVGOUT | wc -l` -gt 0 ]
  then  python3 $APPLYTRANSFORMPY $SVGOUT | sed ":a;N;\$!ba;s/\n/$B/g" | # 
        sed 's/<path/\n<path/g' | sed '/^<path/s/>/>\n/' > ${TMP}.PREFINAL.svg
  else  cat $SVGOUT | sed ":a;N;\$!ba;s/\n/$B/g" | # 
        sed 's/<path/\n<path/g' | sed '/^<path/s/>/>\n/' > ${TMP}.PREFINAL.svg
  fi
# --------------------------------------------------------------------------- #
  (IFS=$'\n';CNT=1
   for P in `grep -n "^<path" ${TMP}.PREFINAL.svg`
   do  N=`echo $P | cut -d ":" -f 1`
       ID=`echo 0000000$CNT | rev | cut -c 1-8 | rev`
       sed -i "${N}s/^<path/& id=\"path$ID\" /" ${TMP}.PREFINAL.svg
       CNT=`expr $CNT + 1`
   done;CNT=1
   for G in `grep -n "<g" ${TMP}.PREFINAL.svg`
   do  N=`echo $G | cut -d ":" -f 1`
       ID=`echo 000$CNT | rev | cut -c 1-4 | rev`
       sed -i "${N}s/<g/& id=\"g$ID\" /" ${TMP}.PREFINAL.svg
       CNT=`expr $CNT + 1`
   done;)
# --------------------------------------------------------------------------- #
  sed ":a;N;\$!ba;s/\n//g" ${TMP}.PREFINAL.svg        | #
  sed 's/>[ ]*</></g' | sed 's/;[ ]*/;/g' | tr -s ' ' | #      
  sed "s/stroke-width:[0-9.px]*/stroke-width:1px/"    | #
  sed "s/$B/\n/g" | sed '/^$/d' > $SVGOUT
# --------------------------------------------------------------------------- #
# INSERT THIS COMMAND INTO SVG OUTPUT
# --------------------------------------------------------------------------- #
  DATE=`date +%d.%m.%Y" "%H:%M:%S`
  THISCMD=`echo "$0 $* ($DATE)" | #
           sed 's/--/-/g'       | #
           sed 's/|/\\\|/g'     | #
           sed 's/^/<!-- /'     | #
           sed 's/$/ -->/'`       # 
  sed -i "1s|^.*$|&\n$THISCMD|" $SVGOUT
# --------------------------------------------------------------------------- #
# C L E A N  U P
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ];then rm ${TMP}*.* ;fi

exit 0;
