#!/bin/bash

# =========================================================================== #
# FILL SHAPES WITH LINES                                                      #
# =========================================================================== #
# Copyright (C) 2021 Christoph Haag                                           #
# --------------------------------------------------------------------------- #
# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details. 
# -> http://www.gnu.org/licenses/gpl.txt

# =========================================================================== #
#  U S E R   I N T E R A C T I O N
# =========================================================================== #  
  SVG=`echo $* | sed 's/ /\n/g' | grep -v "^--" | grep "\.svg$" | head -n 1` 
# --------------------------------------------------------------------------- #
  if [ ! -f $SVG ] ||
     [ `echo "$SVG" | wc -c` -lt 2 ]
   then echo; echo "A SVG FILE PLEASE!"
              echo "e.g. $0 20161110212041.svg --out=output.svg"; echo
        exit 0;
  fi
# --------------------------------------------------------------------------- #
  SVGOUT=`echo $* | sed 's/--/\n&/g' | sed 's/ /\n/g' | #
          grep "^-*-out" | grep "\.svg$" | head -n 1 | cut -d "=" -f 2` 
# --
  if [ `echo $* | sed 's/--/\n&/g' | sed 's/ /\n/g' | #
        grep "^-*-hatch-continuous"  | wc -l` -gt 0  ]
  then HCNTNS="true";else HCNTNS="false";fi
  HINSET=`echo $* | sed 's/--/\n&/g' | sed 's/ /\n/g' | #
          grep "^-*-hatch-inset=" | sed 's/[^0-9]//g'`
# --------------------------------------------------------------------------- #
  if [ "$SVGOUT" == "" ]
  then  echo; echo "DEFINE OUTPUT FILE. PLEASE!"
              echo "e.g. $0 20161110212041.svg --out=output.svg"; echo
        exit 0;fi
  OUTDIR=`dirname $SVGOUT`
  if [ ! -d $OUTDIR ]
  then echo "$OUTDIR DOES NOT EXIST"
       read -p "CREATE $OUTDIR? [y/n] " ANSWER
       if [ "$ANSWER" != "y" ];then echo "Bye";exit 1
                               else mkdir -p $OUTDIR;echo;fi
  fi
  if [ -f $SVGOUT ]
  then echo "$SVGOUT DOES EXIST"
       read -p "OVERWRITE $SVGOUT? [y/n] " ANSWER
       if [ "$ANSWER" != "y" ];then echo "Bye";exit 1;else echo;fi
  fi
# =========================================================================== #
#  C O N F I G U R E
# =========================================================================== #
  TMP="tmp/FLL${RANDOM}"
  INDENTTRACEWIDTH=5000
# --------------------------------------------------------------------------- #
  HOW=`echo $* | sed 's/ /\n/g' |      # GET HOW FILE FROM CLI ...
       grep "\.how$" | head -n 1`      # ... ARGUMENTS (FIRST ONE ONLY)
  cat "$HOW" > ${TMP}.how 2> /dev/null # CREATE FILE (EVEN EMPTY)
  HOW=${TMP}.how                       # USE TMP HOW FILE NOW
# --------------------------------------------------------------------------- #
  NOCOLOR="#ffffff"
  DONTTRACE=`sed -e '/./{H;$!d;}' \
                 -e 'x;/NOT TRACE/!d;/#[0-9a-fA-F]\{6\}/!d' $HOW | #
             sed 's/ /\n/g' | grep "^#[0-9a-f]\{6\}" | sed ':a;N;$!ba;s/\n/|/g'`
  if [ `echo $DONTTRACE | wc -c` -lt 6 ];then DONTTRACE="XXXXXXX";fi
# --------------------------------------------------------------------------- #
  source lib/sh/fill.functions
  source lib/sh/trace.functions
 #source lib/sh/headless.functions
# =========================================================================== #
#  P R E P R O C E S S
# =========================================================================== #
  MEASUREFILLS $SVG ${TMP}.info
# --------------------------------------------------------------------------- #
  B=NL`echo ${RANDOM} | cut -c 1`F00
  S=SP`echo ${RANDOM} | cut -c 1`F0O
  L=LA`echo ${RANDOM} | cut -c 2`F0P
# --------------------------------------------------------------------------- #
# MOVE LAYERS ON SEPARATE LINES (TEMPORARILY; EASIFY PARSING LATER ON)
# --------------------------------------------------------------------------- #
  grep -v "^<!--" $SVG            | # IGNORE COMMENTS
  sed ":a;N;\$!ba;s/\n/$B/g"      | # RM ALL LINEBREAKS (BUT SAVE)
  sed "s/ /$S/g"                  | # RM ALL SPACE (BUT SAVE)
  sed 's/<g/\n<g/g'               | # REDO GROUP OPEN + NEWLINE
  sed "/mode=\"layer\"/s/<g/$L/g" | # PLACEHOLDER FOR LAYERGROUP OPEN
  sed ':a;N;$!ba;s/\n//g'         | # RM ALL LINEBREAKS (AGAIN)
  sed "s/$L/\n<g/g"               | # REDO LAYERGROUP OPEN + NEWLINE
  grep -v 'label="XX_'            | # REMOVE EXCLUDED LAYERS
  sed '/^[ ]*$/d'                 | # RM EMPTY LINES
  sed 's/<\/svg>/\n&/g'           | # PUT SVG CLOSE ON NEW LINE
  tee > ${TMP}.supertmp             # WRITE TO TEMPORARY FILE
# --------------------------------------------------------------------------- #
# IF A STANDARD FILL IS DEFINED REPLACE UNDEFINED FILLS
# --------------------------------------------------------------------------- #
 (STANDARDFILL=`grep  "^[ ]*STANDARD[ ]*FILL" $HOW | #
                sed 's/\(^.*\)[#]*\([a-f0-9]\{6\}\)\(.*\)/\2/I' | #
                head -n 1 | tr [:upper:] [:lower:]`
  if [ "$STANDARDFILL" != "" ];then
   for F in `sed "s/fill:\(#[a-f0-9]\{6\}\)/\n==:\1\n/Ig" ${TMP}.supertmp | #
             grep "^==:" | grep -v "$NOCOLOR" | cut -c 5-11 | sort -u`
    do if [ `grep -i "^[ ]*FILL:$F" $HOW | wc -l` -lt 1 ]
       then sed -i "s/fill:#$F/fill:#$STANDARDFILL/g" ${TMP}.supertmp
       fi
  done;fi;)
# --------------------------------------------------------------------------- #
# IF A STANDARD STROKE IS DEFINED REPLACE UNDEFINED STROKES
# --------------------------------------------------------------------------- #
 (STANDARDSTROKE=`grep  "^[ ]*STANDARD[ ]*STROKE" $HOW | #
                  sed 's/\(^.*\)[#]*\([a-f0-9]\{6\}\)\(.*\)/\2/I' | #
                  head -n 1 | tr [:upper:] [:lower:]`
  if [ "$STANDARDSTROKE" != "" ];then
   for S in `sed "s/stroke:\(#[a-f0-9]\{6\}\)/\n==:\1\n/Ig" ${TMP}.supertmp | #
             grep "^==:" | grep -v "$NOCOLOR" | cut -c 5-11 | sort -u`
    do if [ `egrep -i "^[ ]*#$S[ ]+[0-9][ ]+[0-9][ ]" $HOW | wc -l` -lt 1 ]
       then sed -i "s/stroke:#$S/stroke:#$STANDARDSTROKE/g" ${TMP}.supertmp
       fi
  done;fi;)
# =========================================================================== #
#  P R O C E S S
# =========================================================================== #
# --------------------------------------------------------------------------- #
# CHANGE FILL COLOR DEPENDING ON SIZE
# --------------------------------------------------------------------------- #
 (IFS=$'\n'
  for SIZECOLOR in `grep "^[ ]*SIZECOLOR" $HOW | cut -d '>' -f 2`
   do
      MINW=`echo $SIZECOLOR | cut -d ":" -f 2`
      MINH=`echo $SIZECOLOR | cut -d ":" -f 3`
      MAXW=`echo $SIZECOLOR | cut -d ":" -f 4`
      MAXH=`echo $SIZECOLOR | cut -d ":" -f 5`
      FROMCOLOR=`echo $SIZECOLOR | #
                 cut -d ":" -f 1 | #
                 sed 's/^[ ]*#//'` #
        TOCOLOR=`echo $SIZECOLOR | #
                 cut -d ":" -f 6 | #
                 sed 's/^[ ]*#//'` #
      STROKEOVER=`echo $SIZECOLOR | #
                  cut -d ":" -f 8 | #
                  sed 's/^[ ]*#//'` #
      STROKEOVERNUM=`echo $SIZECOLOR | #
                     cut -d ":" -f 7 | #
                     sed 's/[^0-9]//'` #
      STROKEUNDER=`echo $SIZECOLOR  | #
                   cut -d ":" -f 10 | #
                   sed 's/^[ ]*#//'`  #
      STROKEUNDERNUM=`echo $SIZECOLOR | #
                      cut -d ":" -f 9 | #
                      sed 's/[^0-9]//'` #

      SIZECOLOR --file=${TMP}.supertmp    \
                --fileinfo=${TMP}.info     \
                --minw=$MINW --minh=$MINH   \
                --maxw=$MAXW --maxh=$MAXH    \
                --strokeover=$STROKEOVER      \
                --strokeovernum=$STROKEOVERNUM \
                --strokeunder=$STROKEUNDER      \
                --strokeundernum=$STROKEUNDERNUM \
                --from=$FROMCOLOR --to=$TOCOLOR

  done;)

 #sed -e "s/$B/\n/g" -e "s/$S/ /g" ${TMP}.supertmp > sizecolor.svg; exit 0;

# --------------------------------------------------------------------------- #
# COLLECT PENSIZES FOR COLORS
# --------------------------------------------------------------------------- #
 (IFS=$'\n'
  for PEN in `egrep "^#[ ]*PEN[ ]*[0-9]:" $HOW`
   do
     PENNUM=`echo $PEN | cut -d ":" -f 1 | sed 's/[^0-9]*//g'`
     PENSIZE=`echo $PEN | cut -d ":" -f 2 | sed 's/[^0-9]*//g'`

     for FILL in `egrep -i "^[ ]*FILL:[a-f0-9]{6}" $HOW`
      do
         SRCFILL=`echo $FILL | cut -d "-" -f 1 | #
                  cut -d ":" -f 2 | sed 's/[ ]*//g'`
         PENCOLORFILL=`echo $FILL | #
          sed 's/\(^.*\)#\([a-f0-9]\{6\}\)\(.*\)#\([a-f0-9]\{6\}\)/\2/I'`
         PENNUMFILL=`egrep -i "^[ ]*#$PENCOLORFILL[ ]+[0-9]" $HOW   | #
                     sed 's/^[ ]*//g' | tr -s ' ' | cut -d " " -f 3 | #
                     head -n 1`
         if [ "$PENNUMFILL" == "$PENNUM" ]
          then echo "$SRCFILL:$PENSIZE" >> ${TMP}.indentfill
         fi
     done
  done;)
# --------------------------------------------------------------------------- #
# BUNDLE CONSECUTIVE COLOR ELEMENTS (TO DIFFERENT FILES)
# --------------------------------------------------------------------------- #
  LCNT=1;PCNT=1000
  for LAYER in `cat ${TMP}.supertmp | #
                sed '1d'            | #
                tac | sed '1d'      | #
                tac`                  #
   do 
     LAYEROPEN=`echo $LAYER | sed 's/>/>\n/' | head -n 1`      
   # --------------------------------------------------------------------- #
   # MARK ACCORDING TO COLOR ELEMENTS
   # --------------------------------------------------------------------- #
     CCNT=1
     for ELEMENT in `echo $LAYER  | sed 's/</\n</g' | grep "^<"`
      do
          HASFILL=`echo $ELEMENT | grep "fill:#[a-fA-F0-9]\{6\}" | wc -l`
        HASSTROKE=`echo $ELEMENT | grep "roke:#[a-fA-F0-9]\{6\}" | wc -l`

        FILLCOLOR=`echo $ELEMENT | #
                   sed 's/fill:#[a-fA-F0-9]\{6\}/\n&\n/' | #
                   grep "^fill:#[a-fA-F0-9]\{6\}"`
      STROKECOLOR=`echo $ELEMENT | #
                   sed 's/stroke:#[a-fA-F0-9]\{6\}/\n&\n/' | #
                   grep "^stroke:#[a-fA-F0-9]\{6\}"`

         if [ "$FILLCOLOR" != "$PREVFILLCOLOR" ] || 
            [ "$STROKECOLOR" != "$PREVSTROKECOLOR" ]
         then CCNT=`expr $CCNT + 1`; fi

         if   [ "$HASFILL" -gt 0 ] &&
              [ "$HASSTROKE" -lt 1 ]
         then   #echo "FILL: $FILLCOLOR" 
                echo "$CCNT:$ELEMENT"                >> ${TMP}.${LCNT}.tmp
         elif [ "$HASFILL" -lt 1 ] &&
              [ "$HASSTROKE" -gt 0 ]
         then   #echo "STROKE: $STROKECOLOR"
                echo "$CCNT:$ELEMENT"                >> ${TMP}.${LCNT}.tmp
         elif [ "$HASFILL" -gt 0 ] &&
              [ "$HASSTROKE" -gt 0 ]
         then   #echo "FILL:$FILLCOLOR STROKE:$STROKECOLOR"
                echo "$CCNT:$ELEMENT" | #
                sed 's/stroke:#[a-fA-F0-9]\{6\}/stroke:none/' | #
                tee                                  >> ${TMP}.${LCNT}.tmp
                CCNT=`expr $CCNT + 1`
                echo "$CCNT:$ELEMENT" | #
                sed 's/fill:#[a-fA-F0-9]\{6\}/fill:none/' | #
                tee                                  >> ${TMP}.${LCNT}.tmp
         else   #echo "NOT A COLOR ELEMENT"
                echo "ALL:$ELEMENT"                  >> ${TMP}.${LCNT}.tmp
         fi

         PREVFILLCOLOR="$FILLCOLOR"
         PREVSTROKECOLOR="$STROKECOLOR"
     done
   # --------------------------------------------------------------------- #
   # SORT (TO DIFFERENT FILES) ACCORDING TO MARKS 
   # --------------------------------------------------------------------- #
     for BUNDLE in `cut -d ":" -f 1 ${TMP}.${LCNT}.tmp | #
                    grep "^[0-9]" | sort -un`
      do

        PCNT=`echo 00000000$PCNT | rev | cut -c 1-9 | rev`
        LNUM=`echo 00000000$LCNT | rev | cut -c 1-3 | rev`
        head -n 1 ${TMP}.supertmp >   ${TMP}.${LNUM}.BUNDLE-${PCNT}.00.svg
        egrep "^${BUNDLE}:|^ALL:" ${TMP}.${LCNT}.tmp  | #
        cut -d ":" -f 2-          >>  ${TMP}.${LNUM}.BUNDLE-${PCNT}.00.svg
        echo '</svg>'             >>  ${TMP}.${LNUM}.BUNDLE-${PCNT}.00.svg
        PCNT=`expr $PCNT + 1000`

     done
   # --------------------------------------------------------------------- #
     rm ${TMP}.${LCNT}.tmp

     LCNT=`expr $LCNT + 1`
  done
# --------------------------------------------------------------------------- #
# RESTORE LINEBREAKS AND SPACES
# --------------------------------------------------------------------------- #
  sed -i -e "s/$B/\n/g" -e "s/$S/ /g" ${TMP}.[0-9]*.BUNDLE-*.svg
# --------------------------------------------------------------------------- #
# INDENT FILL ACCORDING TO PENSIZE (IF POSSIBLE)
# --------------------------------------------------------------------------- #
  for FILL in `cat ${TMP}.indentfill 2> /dev/null`
   do 
      PENSIZE=`echo $FILL | cut -d ":" -f 2`
    FILLCOLOR=`echo $FILL | cut -d ":" -f 1`
  # ----------------------------------------------------------------------- #
    for BUNDLE in `grep -il "fill:#$FILLCOLOR" \
                            ${TMP}.[0-9]*.BUNDLE-*0.00.svg`
     do
        BNUM=`echo $BUNDLE    | rev | #
              cut -d "." -f 3       | #
              cut -d "-" -f 1 | rev`  #
        SRCCOLOR=`egrep "^[ ]*SIZECOLOR|^[ ]*F:" $HOW             | #
                  grep $FILLCOLOR | head -n 1 | sed 's/^[ ]*F://' | #
                  cut -d ">" -f 2 | cut -d ":" -f 1               | #
                  sed 's/^[ ]*//' | sed 's/[^#a-f0-9]//I'`          #
        if [ "$SRCCOLOR" == "" ];then SRCCOLOR="none";fi
        LNAME=`sed 's/:label/\n:label/g' $BUNDLE | grep '^:label' | # 
               head -n 1 | cut -d '"' -f 2 | sed 's/[^0-9a-z_:-]//gI'`

        if [ -f "${TMP}.INDENTFILL.svg" ];then rm ${TMP}.INDENTFILL.svg;fi

       (IFS=$'\n'
      # ------------------------------------------------------------------ #
        for P in `sed ':a;N;$!ba;s/\n//g' $BUNDLE | #
                  sed 's/<path/\n<path/g'         | #
                  sed '/^<path/s/>/>\n/'          | #
                  grep "^<path"                   | #
                  egrep 'fill:#[0-9a-f]{6}'`        #
         do
              ID=`echo $P | sed 's/id="/\n&/'     | #
                  grep "^id=" | head -n 1         | #
                  cut -d '"' -f 2`                  #
              MINX=`grep "^${ID}," ${TMP}.info    | #
                    head -n 1 | cut -d "," -f 6`    #
              MINY=`grep "^${ID}," ${TMP}.info    | #
                    head -n 1 | cut -d "," -f 8`    #
 
              if [ "$MINX" -gt "$PENSIZE" ] && [ "$MINY" -gt "$PENSIZE" ]
              then TARGETBUNDLE=${TMP}.INDENTFILL.svg
              else TNUM=`expr $BNUM + 1`
                   TNUM=`echo 00000000$TNUM | #
                         rev | cut -c 1-9 | rev`
                   OLD="-${BNUM}.00.svg";NEW="-${TNUM}.00.svg"
                   TARGETBUNDLE=`echo $BUNDLE | sed "s/${OLD}$/${NEW}/"`
                   P=`echo $P | sed "s/fill:#$FILLCOLOR/fill:$SRCCOLOR/g"`
              fi 
              if [ ! -f "$TARGETBUNDLE" ]
              then head -n 1 ${TMP}.supertmp | #
                   sed  -e "s/$B/\n/g" -e "s/$S/ /g" >  $TARGETBUNDLE
                   echo "<g inkscape:groupmode=\"layer\"  \
                            inkscape:label=\"$LNAME\" \
                         >" | tr -s ' '              >> $TARGETBUNDLE
                   echo '</g></svg>'                 >> $TARGETBUNDLE
              fi
              sed -i "s|</g>|$P\n&|"                    $TARGETBUNDLE
        done;)
      # ------------------------------------------------------------------ #
        if [ `cat ${TMP}.INDENTFILL.svg 2> /dev/null | wc -l` -gt 0 ]
        then  TNUM=`expr $BNUM + 1`
              TNUM=`echo 00000000$TNUM | #
                    rev | cut -c 1-9 | rev`
            # --
              OLD="-${BNUM}.00.svg";NEW="-${TNUM}.00.svg"
              TARGETBUNDLE=`echo $BUNDLE | sed "s/${OLD}$/${NEW}/"`
              cp ${TMP}.INDENTFILL.svg $TARGETBUNDLE
            # --
              PS=`echo 00$PENSIZE | rev | cut -c 1-2 | rev`
              OLD="-${BNUM}.00.svg";NEW="-${TNUM}.${PS}.svg"
              TARGETBUNDLE=`echo $BUNDLE | sed "s/${OLD}$/${NEW}/"`
            # --
              head -n 1 ${TMP}.supertmp | #
              sed  -e "s/$B/\n/g" -e "s/$S/ /g"     >   $TARGETBUNDLE
              echo '</svg>'                         >>  $TARGETBUNDLE
            # --
              cat ${TMP}.INDENTFILL.svg                          | #
              sed "s/stroke:none/stroke:$NOCOLOR/g"              | #
              sed "s/stroke:#[a-fA-F0-9]\{6\}/stroke:$NOCOLOR/g" | #
              sed "s/stroke-width:[0-9.]*//g"                    | #
              sed "s/stroke:$NOCOLOR/&;stroke-width:$PENSIZE/g"  | #
              sed "s/fill:#[a-fA-F0-9]\{6\}/fill:#000000/g"      | #
              tee > ${TMP}.${LNAME}.svg
            # --
              FILLTRACEADD --source=${TMP}.${LNAME}.svg \
                           --target=$TARGETBUNDLE       \
                           --targetfill=$FILLCOLOR      \
                           --tracewidth=$INDENTTRACEWIDTH
        fi
        DELETEBUNDLES="$DELETEBUNDLES $BUNDLE"
    done
  # ----------------------------------------------------------------------- #
  done
# --------------------------------------------------------------------------- #
# RM OBSOLETE BUNDLES
# --------------------------------------------------------------------------- #
  DELETEBUNDLES=`echo $DELETEBUNDLES | sed 's/ /\n/g' | sort -u`
  for D in $DELETEBUNDLES;do if [ -f "$D" ];then rm $D;fi;done
# --------------------------------------------------------------------------- #
# CONVERT FILLS TO LINES
# --------------------------------------------------------------------------- #
  for BUNDLE in `ls ${TMP}.[0-9]*.BUNDLE-*.svg  | #
                 egrep "\.[0-9][0-9]\.svg$"`
   do
      OUTBASE=`echo $BUNDLE | sed 's/[0-9]*\.svg/000000/'`
      ETYPE=`sed "s/fill:#[a-fA-F0-9]\{6\}/\n-&\n/g" $BUNDLE | #
             sed "s/stroke:#[a-fA-F0-9]\{6\}/\n-&\n/g"       | #
             egrep "^-fill|^-stroke" | sed 's/^-//'`           #
      COLOR=`echo $ETYPE | cut -d ":" -f 2 | cut -c 2-7`       #
      ETYPE=`echo $ETYPE | cut -d ":" -f 1`                    #
    # --------------------------------------------------------------------- #
      if [ "$ETYPE" == fill ]
      then for FILLACTION in `grep -in "^[ ]*FILL:$COLOR" \
                              $HOW          |  # FIND FILL ACTIONS
                              cut -d ":" -f 1` # LINENUMBER ONLY
            do
               FILLORDER=`echo 000000$FILLACTION | #
                          rev | cut -c 1-6 | rev`  #
               FILLACTION=`sed -n "${FILLACTION}p" $HOW` 
             # ----
               PENFILLCOLOR=`echo $FILLACTION     | #
                             sed 's/ /\n/g'       | #
                             grep '^#' | head -n 1` #
               PENFILLNUM=`grep "^[ ]*$PENFILLCOLOR" $HOW | head -n 1 | #
                           sed 's/^[ ]*//' | tr -s ' ' | cut -d " " -f 3`   
               PENFILLSIZE=`egrep "^#[ ]*PEN[ ]*${PENFILLNUM}:" $HOW  | #
                            cut -d ":" -f 2 | sed 's/[^0-9]*//g'`
               if [ "$PENFILLSIZE" == "" ];then PENFILLSIZE=0;fi
               PENFILLSIZE=`echo 00$PENFILLSIZE | rev | cut -c 1-2 | rev`
             # ----
               if [ `echo $BUNDLE | # 
                     grep "${PENFILLSIZE}.svg$" | #
                     wc -l` -gt 0 ]
               then  OUT="${OUTBASE}.$FILLORDER."`date +%s%N`".svg"
                     mkFill --in="$BUNDLE" --out="$OUT" --how="$FILLACTION"
               fi
           done
      else cp $BUNDLE "${OUTBASE}.000000."`date +%s%N`".svg"
      fi
    # --------------------------------------------------------------------- #
  done
# --------------------------------------------------------------------------- #
# REASSEMBLE BUNDLES
# --------------------------------------------------------------------------- #
  head -n 1 ${TMP}.supertmp | sed -e "s/$B/\n/g" -e "s/$S/ /g"  > ${TMP}.all
 # ------------------------------------------------------------------------- #
  for BUNDLE in `ls ${TMP}.[0-9]*.BUNDLE-*.svg | #
                 egrep "\.[0-9][0-9]\.svg$"    | #
                 sort -u -t . -k 3,3`            #
   do
      OUT=`echo $BUNDLE | sed 's/[0-9]*\.svg//'`
      LNUM=`echo $BUNDLE | sed "s,^${TMP}\.,," | cut -d "." -f 1`
    # ---------------------------------------------------------------------- #
      if [ "$LASTLNUM" != "" ] &&
         [ "$LNUM" != "$LASTLNUM" ]
      then echo "</G>"                                         >> ${TMP}.all
    # ---------------------------------------------------------------------- #
      fi
    # ---------------------------------------------------------------------- #
      if [ "$LASTLNUM" == "" ] || [ "$LNUM" != "$LASTLNUM" ]
      then LNAME=`sed 's/inkscape:label/\n&/g' ${TMP}.supertmp | #
                  sed "s/$B/\n/g" | grep "^inkscape:label"     | #
                  head -n $LNUM   | tail -n 1 | cut -d '"' -f 2` #
           echo "<G inkscape:groupmode=\"layer\" \
                    inkscape:label=\"$LNAME\"    \
                 >" | tr -s ' '                                >> ${TMP}.all
    # ---------------------------------------------------------------------- #
      fi
    # ---------------------------------------------------------------------- #
      for BUNDLEOUT in `ls ${TMP}* | grep "${OUT}*.[0-9]\{19\}.svg$"`
       do
          sed ":a;N;\$!ba;s/\n/$B/g" $BUNDLEOUT | # RM ALL LINEBREAKS (BUT SAVE)
          sed "s/ /$S/g"                        | # RM ALL SPACE (BUT SAVE)
          sed 's/<g/\n<g/g'                     | # REDO GROUP OPEN + NEWLINE
          sed "/mode=\"layer\"/s/<g/$L/g"       | # PLACEHOLDER FOR LAYERGROUP OPEN
          sed ':a;N;$!ba;s/\n//g'               | # RM ALL LINEBREAKS (AGAIN)
          sed "s/$L/\n<G/g"                     | # REDO LAYERGROUP OPEN + NEWLINE
          sed 's/<\/svg>/\n&/g'                 | # PUT SVG CLOSE ON NEW LINE
          sed '1d' | sed '$d'                   | # RM FIRST+LAST LINE (SVG OPEN/CLOSE)
          sed '/^[ ]*$/d'                       | # RM EMPTY LINES
          sed 's/>/>\n/' | sed '1d'             | # 
          rev | sed 's/</<\n/' | rev | sed '1d' | #
          sed -e "s/$B/\n/g" -e "s/$S/ /g"      | #
          tee                                                  >> ${TMP}.all
      done
    # ---------------------------------------------------------------------- #
      LASTLNUM="$LNUM"
  done
# --------------------------------------------------------------------------- #
  echo "</G></svg>"                                            >> ${TMP}.all 
# --------------------------------------------------------------------------- #
# CLEAN SVG
# --------------------------------------------------------------------------- #
  B=::`echo ${RANDOM} | md5sum | cut -c 1-4`::
  if [ `grep 'transform=' ${TMP}.all | wc -l` -gt 0 ]
  then  python3 $APPLYTRANSFORMPY ${TMP}.all             | #
        sed 's/<\/*g[^>]*>//g'                           | #
        sed 's/<G/<g/g' | sed 's/G>/g>/g'                | #
        sed '/^[ ]*$/d'                                  | #
        sed "s/stroke-width:[0-9.]*/stroke-width:1/g"    > $SVGOUT
  else  cat ${TMP}.all                                   | #
        sed 's/<\/*g[^>]*>//g'                           | #
        sed 's/<G/<g/g' | sed 's/G>/g>/g'                | #
        sed '/^[ ]*$/d'                                  | #
        sed "s/stroke-width:[0-9.]*/stroke-width:1/g"    > $SVGOUT
  fi
# --------------------------------------------------------------------------- #
# APPLY STROKE WIDTHS
# --------------------------------------------------------------------------- #
 (IFS=$'\n'
  for PEN in `egrep "^#[ ]*PEN[ ]*[0-9]:" $HOW`
   do
     PENNUM=`echo $PEN | cut -d ":" -f 1 | sed 's/[^0-9]*//g'`
     PENSIZE=`echo $PEN | cut -d ":" -f 2 | sed 's/[^0-9]*//g'`

     for PENCOLOR in `sed 's/^[ ]*//' $HOW | #
                      egrep -i "^#[0-9a-f]{6}[ ]+[0-9][ ]+$PENNUM" | #
                      cut -d " " -f 1`
      do PENSTYLE=`echo "fill:none;             \
                         stroke:$PENCOLOR;      \
                         stroke-width:$PENSIZE; \
                         stroke-linecap:round;  \
                         stroke-linejoin:round" | #
                   sed 's/ //g'`
         sed -i "/oke:$PENCOLOR/s/yle=\"[^\"]*\"/yle=\"$PENSTYLE\"/" $SVGOUT
    done
  done;)
# --------------------------------------------------------------------------- #
# INSERT THIS COMMAND INTO SVG OUTPUT
# --------------------------------------------------------------------------- #
  DATE=`date +%d.%m.%Y" "%H:%M:%S`
  THISCMD=`echo "$0 $* ($DATE)" | sed 's/--/-/g' | #
           sed 's/^/<!-- /' | sed 's/$/ -->/'`     # 
  sed -i "1s|^.*$|&\n$THISCMD|" $SVGOUT
# --------------------------------------------------------------------------- #
# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi
# --------------------------------------------------------------------------- #

exit 0;
