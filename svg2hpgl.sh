#!/bin/bash

# =========================================================================== #
# TRANSLATE SVG PATHS TO HPGL COORDINATES                                      #
# =========================================================================== #
# Copyright (C) 2021 Christoph Haag                                           #
# --------------------------------------------------------------------------- #
# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details.
# -> http://www.gnu.org/licenses/gpl.txt
# --------------------------------------------------------------------------- #
  OUTDIR="_";PUWAIT=10;
  TMP="tmp/XXXX";HPGLTMP="hpgl.hpgl";SUPERTMP="${TMP}.svg"
  STANDARDORDER="#C0L0RE DRAWMODE PEN SPEED"
# --------------------------------------------------------------------------- #
# I M P O R T S
# --------------------------------------------------------------------------- #
  source lib/sh/shuffle.functions  # NOT SO RANDOM
  source lib/sh/headless.functions # HANDLE X
# --------------------------------------------------------------------------- #
# U S E R   I N T E R A C T I O N
# --------------------------------------------------------------------------- #
  SVG=`echo $* | sed 's/ /\n/g' | grep "\.svg$" | head -n 1`
# --------------------------------------------------------------------------- #
  if [ ! -f $SVG ] ||
     [ `echo "$SVG" | wc -c` -lt 2 ]
   then echo; echo "A SVG FILE PLEASE!"
              echo "e.g. $0 20161110212041.svg"; echo
        exit 0;fi
# --------------------------------------------------------------------------- #
  HPGL="$OUTDIR/"`basename $SVG | sed 's/\.svg$/.hpgl/'`
# --------------------------------------------------------------------------- #
  if [ -f $HPGL ]
  then echo "$HPGL DOES EXIST"
       read -p "OVERWRITE $HPGL? [y/n] " ANSWER
       if [ X$ANSWER != Xy ];then echo "Bye";exit 1;else echo; fi
  fi
# --------------------------------------------------------------------------- #
# C R E A T E   H O W  ( AT LEAST MINIMAL )
# --------------------------------------------------------------------------- #
  HOW=`echo $* | sed 's/ /\n/g' |        # GET HOW FILE FROM CLI ...
       grep "\.how$" | head -n 1`        # ... ARGUMENTS (FIRST ONE ONLY)
  cat "$HOW" > ${TMP}.how 2> /dev/null   # CREATE FILE (EVEN EMPTY)
  HOW=${TMP}.how                         # USE TMP HOW FILE NOW
  echo ""                          >> $HOW
  echo "$STANDARDORDER"            >> $HOW
  echo "STANDARD DRAWMODE  =  1"   >> $HOW
  echo "STANDARD PEN       =  1"   >> $HOW
  echo "STANDARD SPEED     = 10"   >> $HOW
# --------------------------------------------------------------------------- #
# G E T   F L A G S
# --------------------------------------------------------------------------- #
  if [ `echo $* | sed 's/ /\n/g' | #
        grep -- '^--hpdpdxl'     | #
        wc -l` -gt 0 ]
  then  HARDWARE="HPDPDXL"
        echo -e "HARDWARE: \e[34mHP DraftPro DXL\e[39m"
  elif [ `echo $* | sed 's/ /\n/g' | #
          grep -- '^--rolanddxy'   | #
          wc -l` -gt 0 ]
  then  HARDWARE="ROLANDDXY"
        echo -e "HARDWARE: \e[34mRoland DXY-1150A/1250\e[39m"
  else  HARDWARE="NONE"
        echo -e "HARDWARE: \e[34mNONE\e[39m"
  fi
# --
  HPGLSORT=`echo $* | sed 's/ /\n/g'  | #
            grep -- '^--hpgl-sort'`     #
  if [ "$HPGLSORT" != "" ]              #
  then PENSORT=`echo $HPGLSORT        | #
                sed 's/^[^=]*//g'     | #
                cut -d "=" -f 2       | #
                sed 's/[^0-9]/|/g'`     #
       HPGLSORT="Y"
  else PENSORT=`sed 's/ //g' $HOW     | #
                egrep -i 'sort'       | #
                egrep -i 'pen|hpgl'   | #
                sed 's/^[^=]*//g'     | #
                cut -d "=" -f 2       | #
                sed 's/[^0-9]/|/g'`     #
     if [ "$PENSORT" != "" ];then HPGLSORT="Y";fi
  fi;if [ "$HPGLSORT" == "" ];then HPGLSORT="N";fi
# --
  HPGLRNDM=`echo $* | sed 's/ /\n/g'   | #
            grep -- '^--hpgl-rndm'`      #
  if [ "$HPGLRNDM" != "" ]               #
  then PENRNDM=`echo $HPGLRNDM         | #
                sed 's/^[^=]*//g'      | #
                cut -d "=" -f 2        | #
                sed 's/[^0-9]/|/g'`      #
       HPGLRNDM="Y"
  else PENRNDM=`sed 's/ //g' $HOW      | #
                egrep -i 'random'      | #
                egrep -i 'pen|hpgl'    | #
                sed 's/^[^=]*//g'      | #
                cut -d "=" -f 2        | #
                sed 's/[^0-9]/|/g'`      #
     if [ "$PENRNDM" != "" ];then HPGLRNDM="Y";fi
  fi;if [ "$HPGLRNDM" == "" ];then HPGLRNDM="N";fi
# --
  if [ `echo $* | sed 's/ /\n/g'       | #
        grep -- '^--hpgl-wait'         | #
        wc -l` -gt 0 ]                   #
  then HPGLWAIT="Y";else HPGLWAIT="N";fi #
# --
  if [ `echo $* | sed 's/ /\n/g'       | #
        grep -- '^--hpgl-into'         | #
        wc -l` -gt 0 ]                   #
  then HPGLINTO="Y";else HPGLINTO="N";fi #
# --------------------------------------------------------------------------- #
# S E T   V A L U E S
# --------------------------------------------------------------------------- #
  PEN=0 # INIT
  LOPEN=LA`echo ${RANDOM} | cut -c 2`F0P
  B=NL`echo ${RANDOM} | cut -c 2`F00;S=SP`echo ${RANDOM} | cut -c 2`F0O
# --------------------------------------------------------------------------- #
  SVGNAME=`basename $SVG`
  SVGW=`sed 's/ /\n/g' $SVG           | #
        egrep '^width="[0-9\.]+"'     | #
        head -n 1 | cut -d "\"" -f 2  | #
        cut -d "." -f 1`                #
  SVGH=`sed 's/ /\n/g' $SVG           | #
        egrep '^height="[0-9\.]+"'    | #
        head -n 1 | cut -d "\"" -f 2  | #
        cut -d "." -f 1`
# --------------------------------------------------------------------------- #
  if [ "$HARDWARE" == "HPDPDXL" ]
  then # -----------------
       #  HP DraftPro DXL
       # -----------------
         XMAX="2267";YMAX=3543
       # --
         if [ "$SVGW" -lt "$SVGH" ]
         then if [ "$SVGW" -le "$XMAX" ]
              then SCALEFACTOR=1
              else SCALEFACTOR=`python -c "print $XMAX.00 / $SVGW"`
              fi
              ORIENT="PORTRAIT"
         else if [ "$SVGH" -le "$YMAX" ]
              then SCALEFACTOR=1
              else SCALEFACTOR=`python -c "print $YMAX.00 / $SVGH"`
              fi
              ORIENT="LANDSCAPE"
         fi
         TRANSFORM=`echo "transform=\"scale($SCALEFACTOR) \
                          translate(0,0)\" id=\"FIT\""  | #
                    tr -s ' '`
         SCX=$SVGW;SCY=$SVGH
         cp $SVG ${TMP}.FIT.svg;SVG=${TMP}.FIT.svg
         sed -i "s/width=\"$SVGW\"/width=\"$XMAX\"/g"   $SVG
         sed -i "s/height=\"$SVGH\"/height=\"$YMAX\"/g" $SVG
         echo "$XMAX" > w.i ;echo "$YMAX" > h.i
         IPX=`python -c "print $SVGW * 5.6" | cut -d "." -f 1`
         IPY=`python -c "print $SVGH * 5.6" | cut -d "." -f 1`
  elif [ "$HARDWARE" == "ROLANDDXY" ]
  then # -----------------------
       #  Roland DXY-1150A/1250
       # -----------------------
         XMAX=1488;YMAX=1052
       # --
         if [     `echo $SVGW | cut -d "." -f 1` \
              -gt `echo $SVGH | cut -d "." -f 1` ]
         then SCALEFACTOR=`python -c "print $XMAX.00 / $SVGW"`
              TRANSFORM=`echo "transform=\"scale($SCALEFACTOR) \
                               translate(0,0)\" id=\"FIT\""  | #
                         tr -s ' '`
         else SCALEFACTOR=`python -c "print $XMAX.00 / $SVGH"`
              TRANSFORM=`echo "transform=\"rotate(90) \
                               translate(0,-$XMAX)     \
                               scale($SCALEFACTOR)\" id=\"FIT\"" | #
                         tr -s ' '`
         fi; cp $SVG ${TMP}.FIT.svg;SVG=${TMP}.FIT.svg
             sed -i "s/width=\"$SVGW\"/width=\"$XMAX\"/g"   $SVG
             sed -i "s/height=\"$SVGH\"/height=\"$YMAX\"/g" $SVG
         echo "$XMAX" > w.i ;echo "$YMAX" > h.i
  else # -------------
       #  No Hardware
       # -------------
         echo "$SVGW" > w.i ;echo "$SVGH" > h.i
  fi
# --------------------------------------------------------------------------- #
# =========================================================================== #
# P R E P A R E   S V G   +   D R A W I N G   I N  S T R U C T I O N S
# =========================================================================== #
# MOVE SVG LAYERS ON SEPARATE LINES (TEMPORARILY; EASIFY PARSING LATER ON)
# --------------------------------------------------------------------------- #
  sed ":a;N;\$!ba;s/\n/$B/g" $SVG       | # RM ALL LINEBREAKS (BUT SAVE)
  sed "s/ /$S/g"                        | # RM ALL SPACE (BUT SAVE)
  sed 's/<g/\n<g/g'                     | # REDO GROUP OPEN + NEWLINE
  sed "/mode=\"layer\"/s/<g/$LOPEN/g"   | # PLACEHOLDER FOR LAYERGROUP OPEN
  sed ':a;N;$!ba;s/\n//g'               | # RM ALL LINEBREAKS (AGAIN)
  sed "s/$LOPEN/\n<g/g"                 | # REDO LAYERGROUP OPEN + NEWLINE
  sed '/^[ ]*$/d'                       | # RM EMPTY LINES
  sed 's/<\/svg>/\n&/g'                 | # PUT SVG CLOSE ON NEW LINE
  sed 's/display:none/display:inline/g' | # MAKE VISIBLE EVEN WHEN HIDDEN
  tee > ${SUPERTMP}                       # WRITE TO TEMPORARY FILE 
# --------------------------------------------------------------------------- #
# BUNDLE CONSECUTIVE STROKE COLOR ELEMENTS (TO DIFFERENT FILES)
# --------------------------------------------------------------------------- #
  LCNT=1;PCNT=1
  for LAYER in `cat $SUPERTMP | grep "^<g" | grep -v 'label="XX_'`
   do
     LAYEROPEN=`echo $LAYER    | #
                sed 's/>/>\n/' | #
                head -n 1`       #
   # --------------------------------------------------------------------- #
   # MARK ACCORDING TO STROKE COLORS
   # --------------------------------------------------------------------- #
     CCNT=1
     for ELEMENT in `echo $LAYER     | #
                     sed 's/</\n</g' | #
                     grep "^<"`
      do
         COLOR=`echo $ELEMENT | #
                sed 's/\(.*\)\(stroke:#[a-fA-F0-9]\{6\}\)\(.*\)/\2/g' | #
                grep "stroke:#[a-fA-F0-9]\{6\}" | cut -d ":" -f 2-`
         HASCOLOR=`echo $COLOR | grep "[a-fA-F0-9]\{6\}" | wc -l`

         if [ "$HASCOLOR" -ge 1 ] &&
            [ "$COLOR" != "$PREVCOLOR" ]; then 
               CCNT=`expr $CCNT + 1`
         fi
         if [ "$HASCOLOR" -lt 1 ] # = KEEP GROUPS
         then
              echo "ALL:$ELEMENT"    >> ${TMP}.${LCNT}.tmp
         else 
             #echo "$CCNT $COLOR"
              echo "$CCNT:$ELEMENT"  >> ${TMP}.${LCNT}.tmp
         fi
         PREVCOLOR="$COLOR"
     done
   # --------------------------------------------------------------------- #
   # SORT (TO DIFFERENT FILES) ACCORDING TO MARKS 
   # --------------------------------------------------------------------- #
     for BUNDLE in `cut -d ":" -f 1 ${TMP}.${LCNT}.tmp | #
                    grep "^[0-9]" | sort -un`
      do
         PCNT=`echo 000$PCNT | rev | cut -c 1-4 | rev`
         head -n 1 $SUPERTMP     >   ${TMP}.BUNDLE-${PCNT}.svg
         echo  "<g $TRANSFORM>"  >>  ${TMP}.BUNDLE-${PCNT}.svg   
         egrep "^${BUNDLE}:|^ALL:"   ${TMP}.${LCNT}.tmp   | #
         cut -d ":" -f 2-        >>  ${TMP}.BUNDLE-${PCNT}.svg
         echo '</g>'             >>  ${TMP}.BUNDLE-${PCNT}.svg
         echo '</svg>'           >>  ${TMP}.BUNDLE-${PCNT}.svg
         PCNT=`expr $PCNT + 1`
     done
   # --------------------------------------------------------------------- #
     rm ${TMP}.${LCNT}.tmp
     LCNT=`expr $LCNT + 1`
  done
# --------------------------------------------------------------------------- #
# RESTORE LINEBREAKS AND SPACES
# --------------------------------------------------------------------------- #
  sed -i -e "s/$B/\n/g" -e "s/$S/ /g" ${TMP}.BUNDLE-*.svg
# --------------------------------------------------------------------------- #
# COLLECT/CREATE DRAWING INFORMATION (FOR EACH COLOR)
# =========================================================================== #
  for HEXCOLOR in `cat ${TMP}.BUNDLE-*.svg      | # USELESS USE OF CAT
      sed 's/stroke:#[a-fA-F0-9]\{6\}/\n&\n/g'  | # STROKES ON SEPARATE LINES
      grep "^stroke" | cut -d ":" -f 2          | # SELECT/SEPARATE STROKES
      sort -u`                                    # SORT/UNIQ
   do
      HEXNAME=`echo $HEXCOLOR | cut -c 2-7`
    # ------------------------------------------------------------------ #
    # GET VALUES FOR COLOR
    # ------------------------------------------------------------------ #
     (IFS=$'\n';for MATCH in `grep -i "^[ ]*$HEXCOLOR" $HOW | #
                              tr -s ' ' | sed 's/^[ ]*//'`
       do 
          SELECTMATCH=`echo $MATCH | sed 's/ /[ ]*/g'`

          if [ `echo  $MATCH | wc -c` -gt 1 ]
          then  C=2
                for V in `echo $MATCH      | #
                          cut -d " " -f 2- | #
                          sed 's/ /\n/g'`    #
                 do 
                    WHAT=`grep -B 1000 "$SELECTMATCH" $HOW | #
                          grep "#C0L0RE" | tail -n 1       | #
                          tr -s ' ' | sed 's/^[ ]*//'      | #
                          cut -d " " -f $C`

                    G=`grep -i "^[ ]*${WHAT}[ ]*${V}" $HOW | #
                       tail -n 1 | cut -d "=" -f 2 | sed 's/^[ ]*//'`
                    if [ `echo  $G | wc -c` -gt 1 ];then V="$G"; fi
                    VNAME=`echo $WHAT | tr [:upper:] [:lower:]`
                    if [ "$V" != "X" ] && [ "$VNAME" != "pen" ]
                    then echo "$V" >> ${TMP}.${HEXNAME}.${VNAME}
                    else echo "$V" >> ${TMP}.${HEXNAME}.${VNAME}
                    fi
                    C=`expr $C + 1`;G=""
                done
          fi
     done;)
    # ------------------------------------------------------------------ #
    # SET/APPEND STANDARD VALUES
    # ------------------------------------------------------------------ #
      for S in `grep "#C0L0RE" $HOW     | #
                tr -s ' '               | #
                sed 's/^[ ]*//'         | #
                cut -d " " -f 2-        | #
                sed 's/ /\n/g'          | #
                sort -u`
        do      VNAME=`echo $S | tr [:upper:] [:lower:]`
                    V=`grep -i $VNAME $HOW                | #
                       grep -i STANDARD | cut -d "=" -f 2 | #
                       head -n 1 | sed 's/[^0-9]//g'`       #
                G=`grep -i "^[ ]*${S}[ ]*${V}" $HOW | #
                   tail -n 1 | cut -d "=" -f 2 | sed 's/^[ ]*//'`
                if [ `echo  $G | wc -c` -gt 1 ];then V=$G; fi
                if [ ! -f ${TMP}.${HEXNAME}.${VNAME} ]
                then echo "$V" >> ${TMP}.${HEXNAME}.${VNAME}
                fi
      done
  done
# =========================================================================== #
# W R I T E   H P G L   ( COLLECT )
# =========================================================================== #
# START HPGL
# --------------------------------------------------------------------------- #
  if [ "$HARDWARE" == "HPDPDXL" ]
  then # -----------------
       #  HP DraftPro DXL
       # -----------------
         echo "IN;"                                     >  $HPGL
         echo "IP-${IPX},-${IPY},${IPX},${IPY};"        >> $HPGL
         echo "SC$SCX,0,0,$SCY;"                        >> $HPGL
         if [ "$ORIENT" != LANDSCAPE  ]
         then echo "RO90;"                              >> $HPGL
         fi
  elif [ "$HARDWARE" == "ROLANDDXY" ]
  then # -----------------------
       #  Roland DXY-1150A/1250
       # -----------------------
         echo "IN;"                                     >  $HPGL
         # http://www.isoplotec.co.jp/HPGL/eHPGL.htm
         # IP p1x,p1y,p2x,p2y;
         echo "IP0,0,16158,11040;"                      >> $HPGL
         # http://www.isoplotec.co.jp/HPGL/eHPGL.htm
         # SC xmin,xmax,ymin,ymax;
         echo "SC$XMAX,0,0,$YMAX;"                      >> $HPGL
  else # -------------
       #  No Hardware
       # -------------
         echo "IN;"                                     >  $HPGL
  fi
# --------------------------------------------------------------------------- #
# PROCESS ALL INPUT FILES (PREVIOUSLY SEPARATED)
# --------------------------------------------------------------------------- #
  for BUNDLE in `ls ${TMP}.BUNDLE-*.svg`
   do
      echo "$BUNDLE"      > svg.i
      BUNDLECOLOR=`sed 's/stroke:#[a-fA-F0-9]\{6\}/\n&\n/g' $BUNDLE | #
                   grep "^stroke" | cut -d ":" -f 2 | cut -c 2-7    | #
                   tail -n 1`; #echo $BUNDLECOLOR

   MAXSETTINGS=`wc -l ${TMP}.${BUNDLECOLOR}.{pen,speed,drawmode} 2>/dev/null | #
                sort -n | grep -v "total$" | tail -n 1 | #
                sed 's/\(^[ 0-9]*\).*/\1/'`
   if [ "$MAXSETTINGS" == "" ];then MAXSETTINGS=1;fi # FALLBACK
# --------------------------------------------------------------------------- #
# LOOP OVER SETTINGS (PREVIOUSLY SEPARATED)
# --------------------------------------------------------------------------- #
  for I in `seq $MAXSETTINGS`
   do
 # ------------------------------------------------------------------------- #
 # WRITE PARAMETERS (TO HPGL)
 # ------------------------------------------------------------------------- #
   PENSTORE=`ls ${TMP}*.* | grep ${BUNDLECOLOR}.pen`
   if [ -f "$PENSTORE" ]
   then PENPREV="$PEN";PEN=`head -n $I $PENSTORE | tail -n 1`;fi
 # ------------------------------------------------------------------------- #
 # VALIDATE PEN
 # ------------------------------------------------------------------------- #
  if [ `echo $PEN | grep "^[1-9]$" | wc -l ` -eq 1 ];then
 # ------------------------------------------------------------------------- #
   SPEEDSTORE=`ls ${TMP}*.* | grep ${BUNDLECOLOR}.speed`
   if [ -f "$SPEEDSTORE" ]
   then SPEEDPREV="$SPEED";SPEED=`head -n $I $SPEEDSTORE | tail -n 1`;fi
   if [ "$PENPREV" != "$PEN" ];then echo "SP${PEN};" >> $HPGL;fi
   echo "VS${SPEED};" >> $HPGL
 # ----------------------------------------------------------------------- #
 # WRITE PARAMETERS TO A TMP FILE (FOR PROCESSING)
 # ----------------------------------------------------------------------- #
   DRAWMODESTORE=`ls ${TMP}*.* | grep ${BUNDLECOLOR}.drawmode`
   if [ -f "$DRAWMODESTORE" ]
   then DRAWMODE=`head -n $I $DRAWMODESTORE | tail -n 1`
        echo "$DRAWMODE" > drawmode.i                   ;fi
 # ----------------------------------------------------------------------- #
 # TEMPORARILY DISABLE UNREQUIRED INPUT FILES
 # ----------------------------------------------------------------------- #
   SELECTMATCH=`grep -i "^[ ]*#$BUNDLECOLOR" $HOW | #
                head -n $I | tail -n 1            | #
                tr -s ' ' | sed 's/^[ ]*//'       | #
                sed 's/ /[ ]*/g'`                   #
   VTMP=""
   VREQUIRED=`grep -B 1000 "$SELECTMATCH" $HOW | #
              grep "#C0L0RE" | tail -n 1       | #
              tr -s ' '  | sed 's/^[ ]*//'     | #
              sed 's/\(#C0L0RE\)*//Ig'         | #
              sed 's/\(DRAWMODE\)*//Ig'        | #
              sed 's/\(PEN\)*\(SPEED\)*//Ig'   | #
              sed 's/[ ]\+/|/g'                | #
              sed 's/^|//' | sed 's/|$//'`       #
 # ----
   echo "----"
   echo "COLOR:      $BUNDLECOLOR"
   echo "DRAWMODE:   $DRAWMODE"
   echo "PEN:        $PEN"
   echo "SPEED:      $SPEED"
 # ----
   for VSTORE in `ls ${TMP}*.* | grep ${BUNDLECOLOR}.*`
    do
       V=`head -n $I $VSTORE | tail -n 1`
       VNAME=`echo $VSTORE                | #
              rev | cut -d "." -f 1 | rev | #
              egrep -i "$VREQUIRED"`        #
       if [ "$VNAME" != "" ]
       then  echo "$V" > ${VNAME}.i
             echo `echo "${VNAME}:;;;;;;;;;;;;;;;" | #
                   tr [:lower:] [:upper:]          | #
                   cut -c 1-11` $V | sed 's/;/ /g'
             VTMP="$VTMP ${VNAME}.i"
       fi
   done
 # ----
 # ----------------------------------------------------------------------- #
 # WRITE TO FILE WHERE PROCESSING EXPECTS IT (v1.i OR v2.i OR v3.i)
 # ----------------------------------------------------------------------- #
   for N in `grep -i "$DRAWMODE[ ]*=[ ]*" $HOW     | # FIND PATTERN
             head -n 1                             | # SELECT FIRST LINE
             cut -d "=" -f 2                       | # SELECT AFTER EQUAL SIGN
             sed 's/:/\n/g'                        | # SPLIT TO LINES
             sed 's/ //g'                          | # RM SPACES
             grep -n ""`                             # NUMBER LINES
    do
        VNUM=`echo $N | cut -d ":" -f 1`
       VNAME=`echo $N | cut -d ":" -f 2`
       VSTORE=`ls ${TMP}*.* | grep -i ${BUNDLECOLOR}.$VNAME`
       if [ -f "$VSTORE" ]
       then V=`head -n $I $VSTORE | #
               tail -n 1`           #
            echo "${V}" > v${VNUM}.i
       fi
 # ----
  #echo `echo "${VNAME}:;;;;;;;;;;;;;;;" | #
  #      cut -c 1-11` $V | sed 's/;/ /g'
   done
 # ----------------------------------------------------------------------- #
   PENSEGMENT=`grep -i SEGMENT $HOW        | #
               grep -i "PEN[ ]*$PEN"       | #
               grep -i $BUNDLECOLOR        | #
               head -n 1 | cut -d ':' -f 2 | #
               sed 's/[^A0-9\.]*//g'` 
   PENOFFSETX=`grep -i OFFSET $HOW         | #
               grep -i X                   | #
               grep -i "PEN[ ]*$PEN"       | #
               grep -i $BUNDLECOLOR        | #
               head -n 1 | cut -d ':' -f 2 | #
               sed 's/[^0-9\.\-]*//g'` 
   PENOFFSETY=`grep -i OFFSET $HOW         | #
               grep -i Y                   | #
               grep -i "PEN[ ]*$PEN"       | #
               grep -i $BUNDLECOLOR        | #
               head -n 1 | cut -d ':' -f 2 | #
               sed 's/[^0-9\.\-]*//g'` 
   echo $PENSEGMENT > segment.i
   echo $PENOFFSETX > offsetX.i
   echo $PENOFFSETY > offsetY.i
   if [ "$PENOFFSETX" == "" ]
   then PENOFFSETX=0;if [ -f offsetX.i ];then rm offsetX.i;fi;fi
   if [ "$PENOFFSETY" == "" ]
   then PENOFFSETY=0;if [ -f offsetY.i ];then rm offsetY.i;fi;fi
   if [ "$PENSEGMENT" == "" ]
   then PENSEGMENT=X;if [ -f segment.i ];then rm segment.i;fi;fi
 # ----------------------------------------------------------------------- #
   echo "SEGMENT:    $PENSEGMENT"
   echo "OFFSET X/Y: $PENOFFSETX/$PENOFFSETY"
 # ----------------------------------------------------------------------- #
 # RUN PROCESSING
 # ----------------------------------------------------------------------- #
   SKETCHNAME=svg2hpgl

   APPDIR=$(dirname "$0")
   LIBDIR=$APPDIR/lib/processing/2120/$SKETCHNAME/application.linux32/lib
   SKETCH=$LIBDIR/$SKETCHNAME.jar

   CORE=$LIBDIR/core.jar
   GMRTV=$LIBDIR/geomerative.jar
   BTK=$LIBDIR/batikfont.jar

   LIBS=$SKETCH:$CORE:$GMRTV:$BTK

   $VX java -Djava.library.path="$APPDIR" -cp "$LIBS" $SKETCHNAME > /dev/null
 # ------------------------------------------------------------------------- #
 # WRITE HPGL
 # ------------------------------------------------------------------------- #
   if [ -f "$HPGLTMP" ]
   then  uniq $HPGLTMP        >> $HPGL   # COMPENSATE DOUBLED COORDINATES
         rm   $HPGLTMP  
   fi
   echo ""                    >> $HPGL
 # ------------------------------------------------------------------------- #
  else echo "----"
       echo "NOT DRAWING ($BUNDLECOLOR)"
       echo "____"
  fi
 # ------------------------------------------------------------------------- #
 # CLEAR VALUE INPUT FILES
 # ------------------------------------------------------------------------- #
   for VSTORE in $VTMP
    do VSTORE=`echo $VSTORE | grep '\.i$' | grep -v '/'`
       if [ "$VSTORE" != "" ] && [ -f $VSTORE ];then rm $VSTORE;fi
   done
 # ------------------------------------------------------------------------- #
   done
 # ------------------------------------------------------------------------- #
  done
# --------------------------------------------------------------------------- #
# END HPGL
# --------------------------------------------------------------------------- #
  echo "SP0;"                 >> $HPGL
# =========================================================================== #
# MODIFY HPGL
# =========================================================================== #
# COMBINE AND SORT PEN SECTIONS
# --------------------------------------------------------------------------- #
  if [ "$HPGLSORT" == "Y" ]
  then
       sed -i 's/SP0;//g' $HPGL # RM SP0

       if [ "$PENSORT" != "" ]
       then C=1;for P in `echo $PENSORT | sed 's/|/ /g'`
                 do sed -i "s/SP${P};/SP${C}X${P};/g" $HPGL # NUM ORDER
                    C=`expr $C + 1`
                done
                sed -i "s/\(SP\)\([0-9]\);/SP9X\2;/g" $HPGL # HIGHER
       fi

       sed ":a;N;\$!ba;s/\n/$B/g" $HPGL       | # RM NEWLINES (TMPRLY)
       sed "s/SP[1-9X]*;/\n&/g"               | # INSERT NEWLINE BEFORE SP
       grep -n ''                             | # NUMBER ALL LINES
       sed 's/^\([0-9]\+\):\([^;]*;\)/\2\1;/' | # SWITCH LINENUM/SP
       sort -V -t ';' -k 1,2                  | # SORT ACCORDING TO SP/LN
       sed 's/^\([^;]*;\)\([0-9]*;\)/\1/'     | # RM LINENUM
       sed "s/SP[1-9X]*;/&\n/g"               | # INSERT NEWLINE AFTER SP
       grep -n ''                             | # NUMBER ALL LINES
       sort -u -t ':' -k 2                    | # SORT/UNIQ ACCORDING TO SP
       sort -n -t ':' -k 1                    | # RESORT ACCORDING TO LINE NUM
       cut -d ':' -f 2-                       | # RM LINENUM
       sed 's/SP[1-9]X/SP/'                   | # RM PENSORT CHARS
       sed ":a;N;\$!ba;s/\n//g"               | # RM NEWLINES
       sed "s/$B/\n/g" > ${TMP}.hpgltmp         # REDO NEWLINES

       sed '/^$/d' ${TMP}.hpgltmp     >  $HPGL  # MOVE TO $HPGL
       echo "SP0;"                    >> $HPGL  # REDO SP0
       rm ${TMP}.hpgltmp                        # CLEAN UP
  fi
# --------------------------------------------------------------------------- #
# RANDOMIZE DRAWING ORDER (AFTER SELECT PEN) 
# --------------------------------------------------------------------------- #
  if [ "$HPGLRNDM" == "Y" ]
  then  
       for PENSEQ in `sed ":a;N;\\$!ba;s/\n/$B/g" $HPGL | #
                      sed "s/SP[0-9]/\n&/g"`    #
        do 
           SP=`echo $PENSEQ           | #
               sed 's/SP[0-9];/&\n/g' | #
               grep '^SP[0-9];'`        #

         if [ "$SP" != "" ] && 
            [ `echo $SP | egrep "$PENRNDM" | wc -l` -gt 0 ]
         then  
              echo "$SP"                                  >> ${TMP}.hpgltmp
              for SPEEDSEQ in `echo $PENSEQ          | #
                               sed "s/VS[0-9]*/\n&/g"` #
               do
                  VS=`echo $SPEEDSEQ          | #
                      sed 's/VS[0-9]*;/&\n/g' | #
                      grep '^VS[0-9]*;'`        #
                  echo "$VS"                              >> ${TMP}.hpgltmp
                  echo $SPEEDSEQ                   | #
                  sed 's/SP[0-9];//g'              | # 
                  sed 's/VS[0-9]*;//g'             | # 
                  sed "s/\(PU;\)$B\(PA\)/\1\n\2/g" | #
                  shuf --random-source=<(mkseed $SVGNAME) >> ${TMP}.hpgltmp
              done

         else echo $PENSEQ                                >> ${TMP}.hpgltmp
         fi

       done
       sed "s/$B/\n/g" ${TMP}.hpgltmp | sed '/^$/d' > $HPGL
  fi
# --------------------------------------------------------------------------- #
# WAIT A WHILE AFTER PEN UP (= REPEAT THE COORDINATE BEFORE PU; AFTER)
# --------------------------------------------------------------------------- #
  if [ "$HPGLWAIT" == "Y" ]
  then  
       PUWAIT=`seq $PUWAIT | sed "s/^.*$/:/" | sed ':a;N;$!ba;s/\n//g'`
       for LINEPU in `grep -n "^PU;$" $HPGL | cut -d ":" -f 1`
        do
           PREVXY=`sed -n "1,${LINEPU}p" $HPGL | #
                   sed 's/;PA/;\nPA/g'         | #
                   grep "^PA" | tail -n 1`       #
           NEXTXY=`sed -n "$LINEPU,\\$p" $HPGL | #
                   grep "^PA" | head -n 1`       #

            PREVX=`echo $PREVXY | cut -d "," -f 1       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
            PREVY=`echo $PREVXY | cut -d "," -f 2       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
            NEXTX=`echo $NEXTXY | cut -d "," -f 1       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
            NEXTY=`echo $NEXTXY | cut -d "," -f 2       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
     
           CHECKX=`python -c "print abs(int($PREVX) - int($NEXTX))"`
           CHECKY=`python -c "print abs(int($PREVY) - int($NEXTY))"` 
           if [ "$CHECKX" -gt 20 ] || [ "$CHECKX" -gt 20 ]
            then PUXYREPEAT=`echo $PUWAIT | sed "s/:/$PREVXY/g"`
                 sed -i "${LINEPU}s/^PU;$/&$B$PUXYREPEAT/" $HPGL
           fi
       done  
           sed -i -e "s/$B/\n/g" $HPGL # RESTORE LINEBREAKS
  fi
# --------------------------------------------------------------------------- #
# MOVE PEN OUT OF DRAWING AREA BEFORE GOING HOME
# --------------------------------------------------------------------------- #
  if [ "$HPGLINTO" == "Y" ]
  then  
       for LINESP in `egrep -n "^SP[0-8];$" $HPGL | cut -d ":" -f 1`
        do
           PREVXY=`sed -n "1,${LINESP}p" $HPGL | #
                   sed 's/;PA/;\nPA/g'         | #
                   grep "^PA" | tail -n 1`       #
           NEXTXY=`sed -n "$LINESP,\\$p" $HPGL | #
                   grep "^PA" | head -n 1`       #

            PREVX=`echo $PREVXY | cut -d "," -f 1       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
            PREVY=`echo $PREVXY | cut -d "," -f 2       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
            NEXTX=`echo $NEXTXY | cut -d "," -f 1       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #
            NEXTY=`echo $NEXTXY | cut -d "," -f 2       | #
                   sed 's/[^0-9\.]//g' | cut -d "." -f 1` #

           if [ "$NEXTXY" == "" ];then AFTERSP=""
                                  else if [ "$NEXTY" -lt $((YMAX / 2)) ]
                                        then YMOVE="0";else YMOVE="$YMAX"
                                       fi 
                 AFTERSP="PA${XMAX},${YMOVE};${B}PA${NEXTX},${YMOVE};"
           fi
           if [ "$PREVXY" == "" ];then BEFORESP=""
                                  else if [ "$PREVY" -lt $((YMAX / 2)) ]
                                        then YMOVE="0";else YMOVE="$YMAX"
                                       fi 
                 BEFORESP="PA${PREVX},${YMOVE};${B}PA${XMAX},${YMOVE};"
           fi
           sed -i "${LINESP}s/^SP[0-9];$/$BEFORESP$B&$B$AFTERSP/" $HPGL
       done
       sed -i -e "s/$B/\n/g" $HPGL # RESTORE LINEBREAKS
  fi
# =========================================================================== #
# --------------------------------------------------------------------------- #
# INSERT THIS COMMAND INTO HPGL OUTPUT
# --------------------------------------------------------------------------- #
  DATE=`date +%d.%m.%Y" "%H:%M:%S`
  THISCMD=`echo "$0 $* ($DATE)" | #
           sed 's/|/\\\|/g'     | #
           sed 's/^/CO \\\"/'   | #
           sed 's/[ ]*$/\\\";/'`  # 
  sed -i "1s|^.*$|$THISCMD\n&|" $HPGL
# --------------------------------------------------------------------------- #
# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ; fi
  if [ `ls [a-zA-Z]*.i 2>/dev/null | wc -l` -gt 0 ];then rm [a-zA-Z]*.i; fi
# --------------------------------------------------------------------------- #

exit 0;
