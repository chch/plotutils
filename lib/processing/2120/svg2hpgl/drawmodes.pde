
/* -------------------------------------------------------------------------
 DRAWMODE: NOTHING SPECIAL
 ------------------------------------------------------------------------- */

void drawSimple() {

  for (int k = 0; k < pfade.length; k++) {
   grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {
      
      RPoint[] T = grafikPolygon.contours[i].points;
      RPoint[] R = new RPoint[T.length];
      arrayCopy(T, R, T.length-1);
      R[R.length-1] = lastPoint(pfade[k],i);
   // --
      if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }

      for (int j = 0; j < R.length; j++) {        

        x = R[j].x;
        y = R[j].y;

        if (j==0) {
          penAbsolute(x,y,xPrev,yPrev,P);
          penDown();
        } 
        else {      
          penAbsolute(x,y,xPrev,yPrev,P);
        }
        xPrev=x;
        yPrev=y;
      }
      penUp();
    }
  }
}

/* -------------------------------------------------------------------------
 DRAWMODE: REPEAT, (INCREASE SPEED)
 ------------------------------------------------------------------------- */

void drawOften(int repeat) {

  for (int k = 0; k < pfade.length; k++) {
   grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {

      for (int r = 1; r <= repeat; r++) { 

        RPoint[] T = grafikPolygon.contours[i].points;
        RPoint[] R = new RPoint[T.length];
        arrayCopy(T, R, T.length-1);
        R[R.length-1] = lastPoint(pfade[k],i);
     // --
        if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }

        for (int j = 0; j < R.length; j++) {        

          x = R[j].x;
          y = R[j].y;

          if (j==0) { 
            penAbsolute(x,y,xPrev,yPrev,P);
            penDown();
          } 
          else {      
            penAbsolute(x,y,xPrev,yPrev,P);
          }
          xPrev=x;
          yPrev=y;
        }
        penUp();
      }
    }
  }
}

/* -------------------------------------------------------------------------
 DRAWMODE: RANDOM OFFSET
 ------------------------------------------------------------------------- */

void drawOffset(float r) {

  for (int k = 0; k < pfade.length; k++) {
   grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {

      RPoint[] T = grafikPolygon.contours[i].points;
      RPoint[] R = new RPoint[T.length];
      arrayCopy(T, R, T.length-1);
      R[R.length-1] = lastPoint(pfade[k],i);
   // --
      if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }
      
      for (int j = 0; j < R.length; j++) {        

        x = R[j].x; y = R[j].y;

        if ( j%10 != 0 ) {
          randomSeed(int(x + y));
          x = x - (r/10 * random(-1,1)) + (r/10 * random(-1,1));
          y = y - (r/10 * random(-1,1))  + (r/10 * random(-1,1));

        }

        if (j==0) { 
          penAbsolute(x,y,xPrev,yPrev,P);
          penDown();
        } 
        else {      
          penAbsolute(x,y,xPrev,yPrev,P);
        }
        xPrev=x;
        yPrev=y;
      }
      penUp();
    }
  }
}

/* -------------------------------------------------------------------------
 DRAWMODE: BACK AND FORTH
 ------------------------------------------------------------------------- */

void drawBackForth(int repeat) {

  for (int k = 0; k < pfade.length; k++) {
   grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {

      RPoint[] T = grafikPolygon.contours[i].points;
      RPoint[] R = new RPoint[T.length];
      arrayCopy(T, R, T.length-1);
      R[R.length-1] = lastPoint(pfade[k],i);
   // --
      if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }
      
      for (int r = 1; r <= repeat; r++) { 

        for (int j = 0; j < R.length; j++) {        

          x = R[j].x;
          y = R[j].y;

          if (j==0) { 
            penAbsolute(x,y,xPrev,yPrev,P);
            if (r==1) { 
              penDown();
            }
          } 
          else {      
            penAbsolute(x,y,xPrev,yPrev,P);
          }
          xPrev=x;
          yPrev=y;
        }
        if (r==repeat) { 
          penUp();
        }

        /* https://forum.processing.org/one/topic/geomerative-reverse-rpoint-array-how-to.html */
        R = (RPoint[]) reverse(R);
      }
    }
  }
}

/* -------------------------------------------------------------------------
 DRAWMODE: X
 ------------------------------------------------------------------------- */

void drawSteps(int steps) {

  for (int k = 0; k < pfade.length; k++) {
   grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {
      
      RPoint[] T = grafikPolygon.contours[i].points;
      RPoint[] R = new RPoint[T.length];
      arrayCopy(T, R, T.length-1);
      R[R.length-1] = lastPoint(pfade[k],i);
   // --
      if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }

      int nPoints = R.length;

      int increase = (nPoints - 1) / steps;
      if (increase == 0) { increase = nPoints; }
      if (steps >= nPoints) { increase = 1; }

      for ( int o =  min(1 + increase,nPoints);
                o <= nPoints;
                o =  o + min(increase,nPoints) ) {
                  
        if ( o + increase > nPoints ) { 
          o = nPoints;
        }
 
        for (int j = 0; j < o; j++) {

          x = R[j].x;
          y = R[j].y;      

          if (j==0) {
            penAbsolute(x,y,xPrev,yPrev,P);
            penDown();
          }
          else {
            penAbsolute(x,y,xPrev,yPrev,P);
          }
          xPrev=x;
          yPrev=y;
        }
        penUp();
      }
    }
  }
}

/* -------------------------------------------------------------------------
 DRAWMODE: STEPS, BACK/FORTH
 ------------------------------------------------------------------------- */

void drawStepsBackForth(int steps) {

  for (int k = 0; k < pfade.length; k++) {
    grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {

      RPoint[] T = grafikPolygon.contours[i].points;
      RPoint[] R = new RPoint[T.length];
      arrayCopy(T, R, T.length-1);
      R[R.length-1] = lastPoint(pfade[k],i);
   // --
      if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }
      
      int nPoints = R.length;

      int increase = (nPoints - 1) / steps;
      if (increase == 0) { increase = nPoints; }
      if (steps >= nPoints) { increase = 1; }
      int startPoint = 0;

      for ( int o = min(1 + increase,nPoints);
                o <= nPoints;
                o = o + min(increase,nPoints) ) { 

        if ( o + increase > nPoints ) { 
          o = nPoints;
        }

        // FORTH
        for (int j = startPoint; j < o; j++) {        

          x = R[j].x;
          y = R[j].y;      
          if (j==0) { 
            penAbsolute(x,y,xPrev,yPrev,P);
          } 
          else {    
            penAbsolute(x,y,xPrev,yPrev,P);
          }
          if (j == 0 &&
              o <= min(1 + increase, nPoints)) { 
            penDown();
          }
          if (o <= min(1 + increase, nPoints)) { 
            startPoint = 1;
          }

          xPrev=x;
          yPrev=y;
        }
        // BACK
        if ( o < nPoints) {
        for (int j = o - 2; j >= 0; j--) {        
          x = R[j].x;
          y = R[j].y;
          penAbsolute(x,y,xPrev,yPrev,P);
        }
       }
      }
      penUp();
    }
  }
}

/* -------------------------------------------------------------------------
 DRAWMODE: DOTTED LINE
 ------------------------------------------------------------------------- */

void drawDots(int dot,int gap,float dotLength) {

  int P = 5;
  dotLength = map(dotLength,0,100,0,1000);
  RCommand.setSegmentator(RCommand.UNIFORMLENGTH);
  RCommand.setSegmentLength(dotLength);

  boolean drwng = false;

  for (int k = 0; k < pfade.length; k++) {
   grafikPolygon = pfade[k].toPolygon();
    for (int i = 0; i < grafikPolygon.countContours(); i++) {

      RPoint[] T = grafikPolygon.contours[i].points;
      RPoint[] R = new RPoint[T.length];
      arrayCopy(T, R, T.length-1);
      R[R.length-1] = lastPoint(pfade[k],i);
   // --
      if ( decideRevert(R) ) { R = (RPoint[]) reverse(R); }

      int p = 0;
      int nPoints = R.length;

      for (int j = 0; j < R.length; j++) {

        x = R[j].x;
        y = R[j].y;

        if (j==0) {
          penAbsolute(x,y,xPrev,yPrev,P);
        } else {

            if ( p == dot   &&
                 drwng == true ) {
                 penUp();
                 p = 0;drwng = false;
            }
            if ( drwng == true ) {
                 penDown();
                 penAbsolute(x,y,xPrev,yPrev,P);
            }
            if ( p == gap      &&
                 drwng == false ||
                 j > nPoints - dot ) {
                 penAbsolute(x,y,xPrev,yPrev,P);
                 penDown();
                 p = 0;drwng = true;
            }
            xPrev = x;
            yPrev = y;
            p++;
        }
      }
      penUp();
    }
  }
}

/* ------------------------------------------------------------------------- */
