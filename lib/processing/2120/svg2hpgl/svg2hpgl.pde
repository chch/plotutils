import geomerative.*;

PrintWriter output;

float x,y;
float xPrev,yPrev;
float xPrevLine,yPrevLine;
RPolygon grafikPolygon;

String input,drawMode; 

RShape grafik;
RShape[] pfade;

int doRevert,P;
boolean revert;
float v1,v2,v3,segmentLength;
float offsetX,offsetY;

void setup()
{
  size(int(getConf("w.i","1052")),
       int(getConf("h.i","1488")));

  input = getConf("svg.i","xxxxxx");
  //if (match(input,"xxxxxx")!=null) { 
  //println("match");
  //exit(); } // TODO

  RG.init(this);
  RG.ignoreStyles();

     drawMode = getConf("drawmode.i","drawSimple");
      offsetX = float(getConf("offsetX.i","0.0"));
      offsetY = float(getConf("offsetY.i","0.0"));
     doRevert = int(getConf("doRevert.i","0"));
           v1 = float(getConf("v1.i","1"));
           v2 = float(getConf("v2.i","1"));
           v3 = float(getConf("v3.i","1"));
            P = int(getConf("precision.i","1"));

  segmentLength = float(getConf("segment.i","A"));

  if ( !Float.isNaN(segmentLength) ) {

  segmentLength = map(segmentLength,0,100,0,1000);
  RCommand.setSegmentator(RCommand.UNIFORMLENGTH);
  RCommand.setSegmentLength(segmentLength);

  } else {

  RCommand.setSegmentator(RCommand.ADAPTATIVE);

  }

  grafik = RG.loadShape(input);
   pfade = grafik.children;

  output = createWriter("hpgl.hpgl");

  noStroke();

  if ( drawMode == "drawSimple" ) {
    println(drawMode + " -> drawSimple");
    drawSimple();
  } else if (match(drawMode,"drawOften")!=null) {
    println(drawMode + " -> drawOften");
    drawOften(int(v1));
  } else if (match(drawMode,"drawOffset")!=null) {
    println(drawMode + " -> drawOffset");
    drawOffset(v1);
  } else if (match(drawMode,"drawBackForth")!=null) {
    println(drawMode + " -> drawBackForth");
    drawBackForth(int(v1));
  } else if (match(drawMode,"drawStepsBackForth")!=null) {
    println(drawMode + " -> drawStepsBackForth");
    drawStepsBackForth(int(v1));
  } else if (match(drawMode,"drawSteps")!=null) {
    println(drawMode + " -> drawSteps");
    drawSteps(int(v1));
  } else if (match(drawMode,"drawDots")!=null) {
    println(drawMode + " -> drawDots");
    drawDots(int(v1),int(v2),v3);
  } else {
    println(drawMode + " -> drawSimple");   
    drawSimple();
  } 

  output.flush();
  output.close();
  exit();

}
