String getConf (String file, String alternativ) {
  String fromfile;
  File f = new File(sketchPath(file));
  if (f.exists()) {
 //println(loadStrings(f)[0]);
   fromfile = loadStrings(f)[0];
  } else {
  //println("file does not exist");
   fromfile = alternativ;
  }
  return fromfile;
}

boolean decideRevert(RPoint[] R) {
    randomSeed(int(R.length + R[0].x + R[R.length-1].y));
    if ( random(0, 100) < doRevert ) {
      revert = true;
    }
    return revert;
}

RPoint lastPoint(RShape s, int i) {

  RCommand.setSegmentator(RCommand.ADAPTATIVE);
  return s.toPolygon()
          .contours[i]
          .points[s.toPolygon().contours[i]
                   .points.length-1];
}

/* CREATE HPGL COORDINATES (+ VISUALISE AS LINES)
   ---------------------------------------------- */

void penAbsolute (float x, float y,
                  float xPrev, float yPrev,
                  int precision) {

     String X;String Y;
  
     x = x + offsetX;xPrev = xPrev + offsetX;
     y = y + offsetY;yPrev = yPrev + offsetY;

  /* ROUND VALUES TO REDUCE STEPS (HPGL MEMORY)
     REDUNDANT POINTS (=IDENTICAL LINES) REMOVED LATER
     ------------------------------------------------- */
     if (precision == 0) {        x = round(x*0.5)/0.5;
                                  y = round(y*0.5)/0.5;
     } else if (precision == 1) { x = round(x);
                                  y = round(y);
     } else if (precision == 2) { x = round(x*2.00)/2.00;
                                  y = round(y*2.00)/2.00;
     } else if (precision == 3) { x = round(x*4.00)/4.00;
                                  y = round(y*4.00)/4.00;
     } else if (precision == 4) { x = round(x*5.00)/5.00;
                                  y = round(y*5.00)/5.00;
     } else if (precision == 5) { x = round(x*10.0)/10.0;
                                  y = round(y*10.0)/10.0;     
     } else if (precision < 50) { x = round(x*20.0)/20.0;
                                  y = round(y*20.0)/20.0;
     } 

     ellipse(x,y,2,2);
     line(xPrevLine, yPrevLine, x, y);
     xPrevLine = x;yPrevLine = y;

     if ( int(x) == x )      { X = String.format("%.0f",x); } 
     else if (precision > 5) { X = String.format("%.2f",x); } 
     else                    { X = String.format("%.1f",x); }
  // --
     if ( int(y) == y )      { Y = String.format("%.0f",y); }
     else if (precision > 5) { Y = String.format("%.2f",y); } 
     else                    { Y = String.format("%.1f",y); }

     output.println("PA" + X + ","
                         + Y + ";");
}

void penUp ()   { output.println("PU;");noStroke(); }
void penDown () { output.println("PD;");stroke(255,0,0); }
